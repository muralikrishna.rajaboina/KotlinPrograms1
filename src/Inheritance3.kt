open class College{

    fun ECE(){
        println("Branch: 'ECE'")
    }

    fun CSE(){
        println("Branch: 'CSE'")
    }

    fun SectionA(){

        println("Section: 'A' ")
    }
    fun SectionB(){

        println("Secton: 'B' ")
    }
    fun Male(){

        println("Gender: 'MALE' ")
    }

    fun Female(){

        println("Gender: 'FEMALE' ")
    }
    fun GradeO(){

        println("Grade: 'O'" )
    }

    fun GradeA(){

        println("Grade: 'A'" )
    }


}

class Student1(var name:String="Murali",var id:Int=10,var Add:String="Hyderabad"):College() {
    fun Details1() {
        println("Name: $name")
        println("ID: $id")
        println("Address: $Add")
    }
}
class Student2(var name:String="Manaswi",var id:Int=20,var Add:String="Banglore"):College() {
    fun Details2() {
        println("Name: $name")
        println("ID: $id")
        println("Address: $Add")
    }
}
    class Student3(var name: String = "Naveen", var id: Int = 30, var Add: String = "Delhi") : College() {
        fun Details3() {
            println("Name: $name")
            println("ID: $id")
            println("Address: $Add")
        }
    }

    class Student4(var name: String = "Krishna", var id: Int = 40, var Add: String = "Chennai") : College() {
        fun Details4() {
            println("Name: $name")
            println("ID: $id")
            println("Address: $Add")
        }
    }
fun main(Args:Array<String>) {

    println(" Student1 Details")
    val student1 = Student1()
    student1.Details1()
    student1.Male()
    student1.ECE()
    student1.SectionA()
    student1.GradeO()

    println()

    println(" Student2 Details")
    val student2 = Student2()
    student2.Details2()
    student2.Female()
    student2.CSE()
    student2.SectionB()
    student2.GradeA()

    println()

    println(" Student3 Details")
    val student3 = Student3()
    student3.Details3()
    student3.Male()
    student3.ECE()
    student3.SectionA()
    student3.GradeO()

    println()

    println(" Student4 Details")
    val student4 = Student4()
    student4.Details4()
    student4.Male()
    student4.ECE()
    student4.SectionB()
    student4.GradeA()


}