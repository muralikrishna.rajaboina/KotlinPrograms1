open class Animal {
    open var color: String = "red"
    open fun eat() {
        println("this animal was eating")
    }
}

class Dog : Animal() {
    var breed: String = ""
    fun bark() {
        println("Dog was barking")
    }

    override fun eat() {
        super.eat()
        println("dog was eating")
    }

    override var color = "green"
}

class Cat(var age: Int = 6) : Animal() {
    fun meow() {
        println("cat was sounding meow")
    }

    override fun eat() {
        super.eat()
        println("cat was eating")
    }
}

fun main(Args: Array<String>) {

    val dog = Dog()

    println("dog color is ${dog.color}")

    dog.eat()

    dog.breed = "asian"
    println("dog breed is ${dog.breed}")

    dog.bark()

    val cat = Cat()

    // cat.color="brown"
    println("cat color is ${cat.color}")

    cat.eat()


    cat.age = 4
    println("cat age is ${cat.age}")

    cat.meow()

    val animal = Animal()
    animal.color = "white"
    println("animal  color is ${animal.color}")
    animal.eat()

}

