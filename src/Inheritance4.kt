open class College2{

    var total_salary:Float=0F
    var pf=2000F

}
class HOD:College2(){
    var hod_sal: Float = 70000F
    var  hod_bonus:Float=4000F
}

class CSE : College2() {
    var cse_bonus: Float = 60000F
    var cse_sal = 30000F
}
class ECE:College2() {
    var ece_sal=40000F
    var ece_bonus=3000F

}
class EEE:College2(){
    var eee_sal=20000F
    var eee_bonus=3000F
}
fun main(args:Array<String>) {
    val obj1 = EEE()
    obj1.total_salary = obj1.pf + obj1.eee_bonus + obj1.eee_sal
    println("total salary of EEE is ${obj1.total_salary}")
    val  obj2=ECE()
    obj2.total_salary = obj2.pf + obj2.ece_bonus + obj2.ece_sal
    println("total salary of ECE is ${obj2.total_salary}")
    val obj3 = CSE()
    obj3.total_salary = obj3.pf + obj3.cse_bonus + obj3.cse_sal
    println("total salary of CSE is ${obj3.total_salary}")
    val obj4 = HOD()
    obj4.total_salary = obj4.pf + obj4.hod_bonus + obj4.hod_sal
    println("total salary of HOD is ${obj4.total_salary}")
}