sealed class Animal2() {
    fun eat() {
        println("animal was eating")
    }


      class dog3(val n:String= "DOG") : Animal2() {
        fun Bark() {
            println(n +" was barking")
        }

    }

     data class Cat2(val n1: String="CAT", val n2:String="MEOW") : Animal2() {
        fun meow() {
            println(n1+" was sounding "+n2)
        }
    }
}
fun main(Args:Array<String>) {
    var dog:Animal2= Animal2.dog3()
    dog.eat()
    Animal2.dog3().Bark()
    var cat:Animal2= Animal2.Cat2()
    cat.eat()
    Animal2.Cat2().meow()
}

/* sealed class Intention {
    fun sum(){
        println("sum is done")
    }

    data class Refresh(val n1:Int) : Intention(){
        fun eat(){
            println("this is refresh class "+n1)
        }
    }
    data class LoadMore(val n2:Int) : Intention(){
        fun msg() {
            println("this is load more class "+n2)
        }
    }
}
fun main(args: Array<String>) {
    var intent: Intention = Intention.Refresh(10)
    var intent2: Intention = Intention.LoadMore(11)

    intent.sum()


    Intention.Refresh(10).eat()
    Intention.LoadMore(11).msg()
}
// using when operation

sealed class Choice {
   class obj1 : Choice()
   class obj2 : Choice()
}
fun main(args: Array<String>) {
   val obj: Choice = Choice.obj1()

   val output = when (obj) {
      is Choice.obj1 -> "Option One has been chosen"
      is Choice.obj2 -> "option Two has been chosen"
   }

   println(output)
}
*/