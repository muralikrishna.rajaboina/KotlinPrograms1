fun main(args: Array<String>) {
    val number = 373
    var originalNumber: Int
    var remainder: Int
    var result = 0

    originalNumber = number

    while (originalNumber != 0) {
        remainder = originalNumber % 10
        result = result*10+ remainder

        originalNumber /= 10
    }

    if (result == number)
        println("$number is an Pallindrome number.")
    else
        println("$number is not an Pallindrome number.")
}