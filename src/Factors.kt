fun main(args: Array<String>) {
    fun factors(n: Int) {
        println("$n FACTORS:")
        for (i in 1..n) {
            if (n % i == 0) {
                println(i)
            }
        }
    }
    factors(6)
}