fun main(Args: Array<String>) {
    fun max2(x: Int, y: Int): Int {
        return if (x > y) x else {
            y
        }
    }

    val max2 = max2(4, 6)
    println(max2)

    fun max3(x: Int, y: Int, w: Int): Int {
        var z=max2(x,y)
        var k=max2(w,z)
        return k
    }

    val max3 = max3(4, 6, 8)
    println(max3)
}