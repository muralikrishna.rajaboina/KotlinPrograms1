

fun main(Args: Array<String>) {

    println("sum of  and y is " + sum(10,20))
    println("sum of  and y is " + sub(20,10))

    println("sum of  and y is " + sum1(10,20))
    println("sum of  and y is " + sub2(20,10))

    Hello("murali")

    var array= intArrayOf(10, 20, 30, 40)
    array.forEach {eachNum ->println(eachNum)}   //Lambda expression

}


// Normal functions
fun sum (x:Int,y:Int):Int{   // As a Block
    return x+y
}

fun sub (x:Int,y:Int):Int=x-y   // As an Expression

/*Anonymous functions
 Anonymous Functions are like Normal functions but without Name  */
var sum1=fun(x:Int,y:Int):Int{   // As a Block
    return x+y
}

var sub2=fun(x:Int,y:Int):Int=x-y   //As an Expressoin

// Anonymous Functions without return value
var Hello=fun(name:String){
    println("Hello $name")
}
//if anonymus Function last parameter is also is an anonymous function cannot go out side parenthesis only works for Lambdas

