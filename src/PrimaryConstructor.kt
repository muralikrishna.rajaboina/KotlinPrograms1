fun main(Args: Array<String>) {
    var customer2 = Customer2("murali")
    customer2.name
    println(" hello ${customer2.name}") /* main method get value by declaring variable in init block by using "this"
                                       this refers name value murali */
}


class Customer2 constructor(_name: String) //primary constructor
{
       var name:String="krishna"

    init {
        this.name=_name

        println("hello $_name krishna" )  // init block is ececuted after the creation of instance class Customer2
    }
}

//primary constructor does not have any annotations or visibility modifiers, the constructor keyword can be omitted