 fun main(Args: Array<String>) {

     /* If fun has more parameters and last parameter of a function is a lambda expression,
     then the lambda expression can go outside the parentheses */


     var s=sum(10,20){a,b->a+b}    //Outside Parenthesis
     var s2=sum(10,20,{a,b->a+b})  //Inside Parenthesis
     println("sum is:"+ s)

// if only one parameter in a lambda expression then we use "it" instead of parameter
     var s3 = mul(2, {it * 3}) // returns s = 6, it = 2
     println("result: "+ s3)

 }
 fun sum(a: Int, b: Int, c: (Int, Int) -> Int): Int {
     return c(a,b)       //returns a lambda which returns an int
 }

  fun mul(a:Int, c: (Int) -> Int) :Int {
     return c(a)
 }