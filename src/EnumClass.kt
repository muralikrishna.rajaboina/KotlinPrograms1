// enum contains some specific values, these four elements are enum constants
//enum constants are objects  we cannot create objects, basic usage of enum class is type safe enums

enum class Courses{
    JAVA,KOTLIN,ANDROID,JAVASCRIPT
}
//Initialization of enum constants: each enum is instance of enum class

enum  class Colors(var r:Int,var g:Int,var b:Int){
    RED(0,225,40),
    GREEN(225,0,60),
    BLUE(60,225,0)
}
//Abstract class we can override enum constants, enum constants can also declare their own anonymous classes

enum  class Action{
    ACTIVE {
        override fun reverseAction()=  INACTIVE
    },
    INACTIVE {
        override fun reverseAction()= ACTIVE
    };
    abstract fun reverseAction():Action
}

enum class Books( p:Int) {
    BOOK1(100),
    BOOK2(200),
    BOOK3(300),
    BOOK4(400);

 //private  means that member will be accessible on the same level as declaration and all nested declarations.
    private var price:Int= p

    fun getPrice():Int
    {
        return price
    }

}

fun main(Args:Array<String>) {
    println("course name is " + Courses.JAVA)
    println("color is "+ Colors.RED.toString())
    println("color RED g value is "+ Colors.RED.g.toString())

    // every enum constants can have its own properties name and position(ordinal)

    println("color name is "+ Colors.RED.name +" ordinal is "+ Colors.RED.ordinal)
    println("color name is "+ Colors.GREEN.name +" ordinal is " + Colors.GREEN.ordinal)
    println("color name is "+ Colors.BLUE.name +" ordinal is " + Colors.BLUE.ordinal)

    //valueOf methos throws an Illegal Exceptions
    println(Colors.valueOf("BLUE"))

    for(ColorsValue in Colors.values()){
        println(" colors values are " + ColorsValue)
    }

    println(Action.ACTIVE.reverseAction())
    println(Action.INACTIVE.reverseAction())

    var c1:Books
    println("print all Books price:")
    for(c1: Books in Books.values()){
        println(  "book name:"+ c1  + "  price: " +c1.getPrice())
    }
}