import org.w3c.dom.ranges.Range

fun main(args: Array<String>) {

    fun Range(n: Int) {
        println("Rangr from 1 to $n")
        for (i in 1..n) { // equivalent of 1 <= i && i <= 10
            println(i)
        }
    }

        Range(5)
      println()

        fun factors(n: Int) {
            println("$n FACTORS:")
            for (i in 1..n) {
                if (n % i == 0) {
                  println(i)
                }
            }
        }
    factors(6)
}