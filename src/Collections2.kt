// IMMUTABLE and MUTABLE lists in kotlin

fun main(Args: Array<String>) {

    // IMMUTABLE Collections -> listOf
   var MyList1 =listOf<String>("MURALI","KRISHNA","NAVEEN","SAI")  // IMMUTABLE, FIXED SIZE and Read Only
   var MyList2= listOf<Int>( 1,2,3,4,5)
    for (number in MyList1) {   //using individual Element
        println(number)
    }
    println()

    //  printing all values by using  Index in For loop
    for(index in 0..MyList2.size-1) {
        println(MyList2[index])
    }
    println()


   //MUTABLE Collections ->mutableListOf, ArrayList, or arrayListOf ( either of these three we can use any)

    var MyList3 = mutableListOf<String>("MURALI","BALU")  // MUTABLE,  No FIXED SIZE and can Add or Remove Elements
    var MyList4 = arrayListOf<String>("MURALI","BALU")
    var MyList5 = ArrayList<String>()
    MyList3.add("RAM")
    MyList3.add("KRISHNA")     // Adding "KRISHNA" to list
    MyList3[0]="SAI"           // Mutable  Replacing 0 index
    MyList3.remove("BALU")  // Removing Element vfrom list

    for(index in 0..MyList3.size-1) {
        println(MyList3[index])
    }
}



