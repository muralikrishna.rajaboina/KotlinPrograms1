
open class Pattern {

    fun triangle1() { //super class method 1
        val rows = 5
        var k = 0
        for (i in 1..rows) {
            for (space in 1..rows - i) {
                print("  ")
            }

            while (k != 2 * i -1) {
                print("* ")
                k++
            }

            println()
            k=0
        }

    }

    fun triangle2() { //super class method 2
        val rows = 5

        for (i in rows downTo 1) {
            for (j in 1..i) {
                print("* ")
            }
            println()
        }

    }
}
    class Shape1 : Pattern() {   //Derived class 1
        fun square1() {
            val rows = 5

            for (i in 1..rows) {
                for (j in 1..i) {
                    print("@ ")
                }
                println()
            }
        }

    }

    class Shape2 : Pattern() {   //Derived class 2
        fun square2() {
            val rows = 5

            for (i in 1..rows) {
                for (j in 1..rows) {
                    print("$j ")
                }
                println()
            }
        }

    }

    fun main(Args: Array<String>) {
        var shape1 = Shape1()
        println("print Derived class 1")
        shape1.square1()
        shape1.triangle1()

        var shape2 = Shape2()

        shape2.square2()
        shape2.triangle2()

    }
