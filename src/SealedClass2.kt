sealed class Number






    data class FloatVal(val number: Float) : Number()
    data class Sum(val n1: Number, val n2: Number) : Number()
    data class Sub(val n1: Number, val n2: Number) : Number()
    data class Mul(val n1: Number, val n2: Number) : Number()


   // object NotANumber : Number()


    fun eval(obj: Number): Float = when (obj) {
        is FloatVal -> obj.number
        is Sum -> eval(obj.n1) + eval(obj.n2)
        is Sub -> eval(obj.n1) - eval(obj.n2)
        is Mul -> eval(obj.n1) * eval(obj.n2)
       //  NotANumber -> Float.NaN
    }


    fun main(Args: Array<String>) {
        val n1 = FloatVal(10.4F)
        val n2 = FloatVal(12.5F)
        println("n1=" + n1.number)
        println("n2=" + n2.number)

        val sum = Sum(n1,n2)
        val sub = Sub(n1,n2)
        val mul = Mul(n1,n2)
        println(eval(sum))
        println(eval(sub))
        println(eval(mul))





    }
