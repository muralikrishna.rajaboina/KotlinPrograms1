fun main(Args:Array<String>){
    var customer=Customer1()         //it is an object
    customer.name="murali"           // passing value to variable name
    println( "hello  ${customer.name} krishna " )   //print by using $ symbol


}
class Customer1
{
    var name:String="surya" // default value passing to variable name
    init{
        println("hello "+name)     // init block is immidiatly executed after the creation  of instance of Custumer1 class
    }
}


// O/P ->  1. hello surya
        // 2.hello murali krishna

// because init block  will be ececuted bofore the main method





