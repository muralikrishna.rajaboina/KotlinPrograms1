open class Animal1(var color:String) { //super primary constructor class
    init{
        println("the dog  color is $color" )
    }
}
class Dog1(var breed:String, color:String) : Animal1(color) {  //derived class secondary
    init{
        println("the dog breeding is $breed and color is $color" )
    }
}
fun main(Args:Array<String>) {
    var dog = Dog1("japan", "blue")

}