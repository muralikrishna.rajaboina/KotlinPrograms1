//     ARRAYS in kotlin  -> Collection of Similar data types like int,String,float,double..etc
//     Elements 1 8 1 1 6
//     Index    0 1 2 3 4     -> Size 5

fun main(Args: Array<String>) {
    // Arrays are fixed size in nature 5 is array size
    var MyArray = Array<Int>(5) { 1 }   // {1} is lambda expression passing all values of array is "1"
    println(MyArray[2])     //prints Arrray element of index 2
    println(MyArray[3])
    MyArray[4] = 6     // Arrays are mutable we can change Array values
    MyArray[1] = 8

    //  printing all values by using For loop
    for (number in MyArray) {   //using individual Element
        println(number)
    }
    println()

   //  printing all values by using  Index in For loop
    for(index in 0..MyArray.size-1) {
    println(MyArray[index])
    }
}

// Arrays are FIXED SIZE and MUTABLE