//interface Base1{
//    fun shape()
//    {
//        print("which shape")
//    }
//}
//class BaseImpl1(val w:Int, val x:Int,val y:Int, val z:Int):Base1 {
//    override fun shape() {
//        if (x == y && z == w && x == w) {
//            print("this is square")
//        } else {
//            print("this is rectangle")
//
//
//        }
//    }
//}
//
//class Deligate1(b:Base1):Base1 by b
//fun main(Args:Array<String>) {
//
//    val b = BaseImpl1(4,4,4,4)
//    Deligate1(b).shape()
//}


data class Movie(var name: String, var studio: String, var rating: Float)
//
val movie = Movie("Whiplash", "Sony Pictures", 8.5F)
fun main(args:Array<String>) {
    println(movie.name)   //Whiplash
    println(movie.studio) //Sony Pictures
    println(movie.rating) //8.5
    movie.rating = 9F
    println(movie.toString())
}
 // Movie(name=Whiplash, studio=Sony Pictures, rating=9.0)


val betterRating = movie.copy(rating = 9.5F)
//fun main(args:Array<String>) {//copy function
//    // println(betterRating.toString())
//// Movie(name=Whiplash, studio=Sony Pictures, rating=9.5)
//
////destructuring declarations
//    movie.component1() // name
//    movie.component2() // studio
//    movie.component3() // rating
//
//
////creating multiple variables from the object
//    val (name, studio, rating) = movie
//
//    fun getMovieInfo() = movie
//    val (namef, studiof, ratingf) = getMovieInfo()
//}
////}