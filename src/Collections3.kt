fun main(Args: Array<String>) {
// IMMUTABLE Collections -> mapOf

    // mapOf contains key and value pair
    var Mymap1 =mapOf<Int,String>(1 to "murali", 2 to "ram",4 to "krishna")  // IMMUTABLE, FIXED SIZE and Read Only

for(key in Mymap1.keys)      // returns key values in Mymap1
{                            // using individual element (object)
   println("Element at key $key: is ${Mymap1[key]}")      // we will also use Mymap1.get(key)
}
    println()


    //MUTABLE Collections ->mutableMapOf, HashMap, or hashMapOf ( either of these three we can use any)

    var Mymap2 =hashMapOf<String,Int>("murali" to 10, "ram" to 20,"krishna" to 30) // MUTABLE,No FIXED SIZE, Read and Write operations
    var Mymap3 =HashMap<String,Int>()
    var Mymap4 =mutableMapOf<String,Int>("shiva" to 60, "ramu" to 70)
    Mymap2["murali"]=5      //changing value of key "murali"
    Mymap2.put("sai",40)    // adding  value 40 of key "sai"
    Mymap2.remove("ram",20)  //removing key value pair in map("ram to 20")

    for(key in Mymap2.keys)
    {
        println("Person Name $key: and Id: ${Mymap2.get(key)}")      // we will also use Mymap1.get(key)
    }

}
