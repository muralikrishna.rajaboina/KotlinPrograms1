
  data class User(var name:String, var id:Int)

  data class Student( val a :String,val b: Int ){
      var name:String = a
      var price:Int = b
  }

  data class Forecast(var name:String ){


  }



 fun main(Args: Array<String>) {
     var user1=User("murali",10)
     var user2=User("murali",10)

     println(user1.toString())  // toString
     println(user1.equals(user2))

     var newuser=user1.copy("krishna")  //Copy

     println(newuser)

     var newuser2=user1.hashCode()   //HashCode
     println(newuser2)


     if(user1==user2){     //Compare
         println("both values are equal")
     }
     else{
         println("values are not equal")
     }

     // Destructing Declaration

     val s = Student("BOOK",500)
     val (name,price) = s
     println("this is my  "+ name +  " its cost is: "+ price)

  //component functions =>to access individual properties of data class

     var book1 = User("The Last Sun",250)
     var name1 = book1.component1();
     var id1 = book1.component2();
     println("$name1 has a id of $id1")


 }
