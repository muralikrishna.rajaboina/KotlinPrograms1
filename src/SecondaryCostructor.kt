fun main(Args: Array<String>) {
    var customer = Customer("murali", 10)
    print("customer name is ${customer._name} and id is  ${customer.id}")
}


class Customer constructor(var _name: String) //primary constructor  _name can behave as property of Customer class
{

    var id: Int? = 12   //declaring secondary constructor parameter

    init {
        println("hello $_name your id is $id")
    }

    //  we can call primary constructor from secondary using this keyword


    constructor(n: String, id: Int) : this(n) //secondary constructor 'id' cannot behave as property of Customer class
                               // so we cannot declare 'id' using var or val keyword inside secondary constructor
     {
        this.id = id
         //we can accees id using this keyword
    }
}

// The body ofsecondary constructor called  after the  init block
