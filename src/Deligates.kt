interface Base{
    fun sum()
}
class BaseImpl(val x:Int, val y:Int):Base
{
    override fun sum(){
        println("x="+x)
        println("y="+y)
        println( "sum="+(x+y))

    }
}
class Deligate(b:Base):Base by b
fun main(Args:Array<String>)
{

    val b =BaseImpl(4,6)
    Deligate(b).sum()


}