class Lambda{

    fun add( x:Int, y:Int,action:(Int) ->Unit ){
        var sum =x+y
        var res=action(sum) //replaced by println(sum)

    }
    fun add2(a:Int, b:Int,action2:(Int,Int) ->Unit ){

        // println(sum)
         action2(a,b) //replaced by println(sum)
    }
   // fun msg(str:String,action2:(String) ->Unit ){

        // println(sum)
   //     action2(str) //replaced by println(sum)
  //  }
}
fun main(Args: Array<String>) {
    var lambda =Lambda()
    // val myLambda:(Int) ->Unit = {s:Int -> println(s)}    //Lambada expression method 1
   // lambda.add(4,6,{s:Int -> println(s)})      method2
    lambda.add(4,6){s:Int -> println(s)}     //mrthod3

    var lambda2 =Lambda()
    var result=0
    val myLambda2:(Int,Int) ->Unit = {x,y ->result=x+y}
    lambda2.add2(2,3,myLambda2)
    println(result)

    //val myLambda2:(Int,Int) ->Unit = {"murali" ->it.reverse()}
  //  lambda2.add2(2,3,{"murali"It.reverse()})
   // println(result)

}