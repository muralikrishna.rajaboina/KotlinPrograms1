//Array is mutable and fixed size

//fun main(args: Array<String>) {
//    var args=Array<String>(5) {"fff"}
//    if (args.size == 0) {
//        println("Please provide a name as a command-line argument")
//        return
//    }
//    println("Hello, ${args}!")
//    for (name in args)
//        println("Hello, $name!")
//
//}
//fun main(args: Array<String>) {
//    var args = Array<String>(5) { "fff" }
//    val language = if (args.size == 0) "FR" else args[0]
//    println(when (language) {
//        "EN" -> "Hello!"
//        "FR" -> "Salut!"
//        "IT" -> "Ciao!"
//        else -> "Sorry, I can't greet you in $language yet"
//    })
//}
fun main(args: Array<String>) {
    println(max(
            args[0].toInt(),
            b = args[1].toInt()
    ))
}

var a=12
var b=3
fun max(a: Int, b: Int) = if (a > b) a else b
