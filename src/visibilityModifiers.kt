fun main(Args: Array<String>) {
    var subclass=Subclass()
    var out=Outer()
    // println("value is  "+ out.a)    cannot accees a is private in Outer
    // println("value is  "+ out.b)    cannot accees a is private in Outer
    println("value is  "+ out.c)
    println("value is  "+ out.d)




    //  println("value is  "+ subclass.a )  // cannot access  'a'because it is invisible  in Subclass
    //  println("value is  "+ subclass.b )  //  cannot access  'b' because it is protected in Subclass
    println("value is  "+ subclass.c )
    println("value is  "+ subclass.d )

}



open class Outer {
    private var a:Int = 1
    protected open val b = 2
    internal var c = 3
    val d = 4  // public by default



    protected class Nested {
        val k: Int = 5
        init{
            println(k)
        }
    }
}

class Subclass : Outer() {


    // override  val a=7    a is not visible so it cannot be overriden

    override val b = 5


    init{
        //  println(a)    a is not visible
        println(b)    // b, c and d are visible
        println(c)
        println(d)      // Nested and e are visible


    }

    // 'b' is protected
}

class Unrelated(o: Outer) {


    init{
        //   println(o.a)
        //    println(o.b)   // o.a, o.b are not visible
        println(o.c)
        println(o.d) // o.c and o.d are visible (same module)

    }

    // Outer.Nested is not visible, and Nested::e is not visible either
}