fun main(Args: Array<String>) {
    fun powerOf(x:Int,y:Int) {
        var z = 1

        if (y === 0) {
            println(" $x Power $y is: 1  ")
        } else {
            for (i in 1..y) {
                z = z * x
            }
            println("$x Power $y is: $z")
        }
    }
    powerOf(4,3)
}